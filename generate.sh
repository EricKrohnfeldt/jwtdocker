#!/bin/sh

set -e

./validate.sh

USER_ID=$(uuidgen)
#USER_ID="4bdb8b8f-0a61-40ff-8f3c-87e5d581e526"
TENANT_ID="46300550-a024-4eee-8643-51bab3690d3a"
#TENANT_ID="ff7777ae-432a-4822-984f-b4feec7ab6b1"
EXPIRE_TIME=$( date -d "1 day" +%s )
EXPIRE_TIME_HUMAN=$( date -d @${EXPIRE_TIME} )

cd /work/project

ROLES='"Administrator"'
if [[ -f jwtRoles.txt ]]; then
  ROLES="\"$( sed 's/#.*$//' jwtRoles.txt | sed '/^[[:space:]]*$/d' | paste -s -d, - | sed 's/[[:space:]]*,[[:space:]]*/","/g' )\""
fi

JWT_HEADER_JSON='{"typ":"JWT","alg":"RS256"}'
JWT_PAYLOAD_JSON=$( echo '{
    "tenantId": "'${TENANT_ID}'",
    "userId": "'${USER_ID}'",
    "username": "Rick",
    "exp": '${EXPIRE_TIME}',
    "authorities": ['${ROLES}']
}' | jq -c '' )

urlEncode() {
  echo -n "$1" | base64 --wrap=0 | sed 's/\\+/-/g' | sed 's/\//_/g' | sed -E 's/=+$//'
}

sign() {
  echo -n "$1" |
   openssl dgst -binary -sha256 -sign /work/project/certificates/client.unencrypted.key |
   openssl enc -base64 |
   tr -d '\n=' |
   tr -- '+/' '-_'
}

  # Sed adds URL encoding
JWT_HEADER=$( urlEncode "${JWT_HEADER_JSON}" )
JWT_PAYLOAD=$( urlEncode "${JWT_PAYLOAD_JSON}" )
JWT_SIGNATURE=$( sign "${JWT_HEADER}.${JWT_PAYLOAD}" )

CERT="${JWT_HEADER}.${JWT_PAYLOAD}.${JWT_SIGNATURE}"

echo
echo "User: ${USER_ID}"
echo "Tenant: ${TENANT_ID}"
echo "Expires: ${EXPIRE_TIME_HUMAN}"
echo "Roles ${ROLES}"
echo
echo Cert: ${CERT}
echo

if [ -f docker-compose.yml ]; then
  sed -i "s/USER_ID:.*/USER_ID: ${USER_ID}/" docker-compose.yml
  sed -i "s/INTEGRATION_TEST_USERID:.*/INTEGRATION_TEST_USERID: ${USER_ID}/" docker-compose.yml
  sed -i "s/JWT_TOKEN:.*/JWT_TOKEN: ${CERT}/" docker-compose.yml
  echo 'Docker-compose replacement complete'
else
  echo 'Skipping docker-compose substitution'
fi


