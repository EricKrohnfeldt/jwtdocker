#!/bin/bash
set -e
cd "$( dirname "$0" )"
IMAGE_NAME='generatejwt'
IMAGE_VERSION=$(date +%s)
docker build -t "$IMAGE_NAME" -f Dockerfile .
docker tag "$IMAGE_NAME":latest docker-dev.teledev.io/"$IMAGE_NAME":"$IMAGE_VERSION"
docker tag "$IMAGE_NAME":latest docker-dev.teledev.io/"$IMAGE_NAME":latest
docker push docker-dev.teledev.io/"$IMAGE_NAME":"$IMAGE_VERSION"
docker push docker-dev.teledev.io/"$IMAGE_NAME":latest

