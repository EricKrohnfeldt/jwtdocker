FROM alpine

RUN apk add --update coreutils
RUN apk add --no-cache \
    openssl \
    util-linux \
    jq \
    bash

WORKDIR /work
COPY validate.sh /work
COPY generate.sh /work
RUN chmod u+x validate.sh generate.sh

CMD ["/bin/sh","/work/generate.sh"]
