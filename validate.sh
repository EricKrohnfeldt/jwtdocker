#!/bin/sh

set -e

if ! [ -d /work/project ]; then
  echo 'Project folder required'
  echo 'Map project volume: -v {PROJECT_FOLDER_LOCATION}:/work/project'
  exit 1
elif ! [ -f /work/project/certificates/client.unencrypted.key ]; then
  echo 'Please create certificates'y
  echo 'run certificates/create-certs.sh'
  exit 2
fi
